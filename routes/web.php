<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/','HomeController@index')->name('root');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/products','ProductController');
Route::resource('/shoppingCart','ShoppingCartController');
Route::name('admin.')->group(function () {
  Route::group(['prefix' => 'dashboard',  'middleware' => 'auth'],function () {
    Route::get('home','Admin\AdminController@home')->name('home');
    Route::resource('categories','Admin\CategoryController');
    Route::resource('products','Admin\ProductController');
  });
});
Route::get('/clearCart', 'ShoppingCartController@clearShoppingCart')->name('clearShoppingCart');

Route::post('/checkout', 'OrderController@payWithAuthorize')->name('checkout');

Route::get('/shipping', 'OrderController@shipping')->name('shipping');

Route::post('/shippment', 'OrderController@shippmentDetails')->name('shippment');

Route::get('payPal', 'OrderController@payWithPaypal')->name('payPal');

Route::get('payments', 'OrderController@payments')->name('payments');

Route::get('paymentStore', 'OrderController@storePayment')->name('paymentStore');

Route::get('contact', 'HomeController@contact')->name('contact');

Route::post('contactMessage', 'HomeController@contactMessage')->name('contactMessage');

Route::post('guestRegistration', 'Auth\RegisterController@guestRegistration')->name('guestRegistration');
