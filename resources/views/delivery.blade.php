@extends('layouts.master')
@section('content')
  @if (Session::has('status'))
   <div class="d-flex justify-content-center alert alert-success">
    <h4>{{session::get('status')}}</h4>
   </div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
               @endforeach
           </ul>
    </div>
  @endif
  <div class="container">
    <form class="" action="{{route('shippment')}}" method="post">
      {{ csrf_field() }}
    <div class="col-md-12">
       @if ($userShippmenet == null)
         <div class="row">
           <div class="form-group col-md-6">
             <label for="">Full name</label>
             <input type="text" required name="fullName" value="{{old('fullName')}}" class="form-control" id="" placeholder="First Last">
           </div>
           <div class="form-group col-md-6">
             <label for="">Phone #</label>
             <input type="text" required name="phone" value="{{old('phone')}}" class="form-control" id="" placeholder="Please enter your phone">
           </div>
           <div class="form-group col-md-6">
             <label for="">City</label>
             <input type="text" required name="city" value="{{old('city')}}" class="form-control" id="" placeholder="Please enter your city">
           </div>
           <div class="form-group col-md-6">
             <label for="">Address</label>
             <input type="text" required name="address" value="{{old('address')}}" class="form-control" id="" placeholder="Please enter your address">
           </div>
         </div>

       @else
         <div class="row">
           <div class="form-group col-md-6">
             <label for="">Full name</label>
             <input type="text" required name="fullName" value="{{$userShippmenet->fullName}}" class="form-control" id="" placeholder="First Last">
           </div>
           <div class="form-group col-md-6">
             <label for="">Phone #</label>
             <input type="text" required name="phone" value="{{$userShippmenet->phone}}" class="form-control" id="" placeholder="Please enter your phone">
           </div>
           <div class="form-group col-md-6">
             <label for="">City</label>
             <input type="text" required name="city" value="{{$userShippmenet->city}}" class="form-control" id="" placeholder="Please enter your city">
           </div>
           <div class="form-group col-md-6">
             <label for="">Address</label>
             <input type="text" required name="address" value="{{$userShippmenet->address}}" class="form-control" id="" placeholder="Please enter your address">
           </div>
         </div>
       @endif
      {{-- <div class='card col-md-6'>
        <div>
          Pay securely Using your credit card.
        </div>
        <div class="card-body">
          <div class="row">
            <div class="form-group col-md-12">
              <label for="">Card Number *</label>
              <input type="text" required name="cardNumber" value="{{old('address')}}" class="form-control" id="" placeholder="**** **** **** ****">
            </div>
            <div class="form-group col-md-6">
              <label for="">Expiry (MM/YY) *</label>
              <input type="text" required name="expiry" value="{{old('address')}}" class="form-control" id="" placeholder="**** **** **** ****">
            </div>
            <div class="form-group col-md-6">
              <label for="">Card Code *</label>
              <input type="text" required name="cardCode" value="{{old('address')}}" class="form-control" id="" placeholder="**** **** **** ****">
            </div>
          </div>
        </div>
      </div> --}}
      <div class="text-center">
        <button type="submit" class="btn btn-secondary btn-block">Proceed</button>
      </div>
    </div>

  </form>

  </div>
<script type = "text/javascript">
function validation() {
  if (confirm("Are you sure want to delete?")) {
         return true;
  }
         return false;
}
});
</script>
@endsection
