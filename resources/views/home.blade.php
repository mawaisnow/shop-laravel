@extends('layouts.master')

@section('content')
  @php
     $tempIterator = 0;
  @endphp
  <div class="container">
    <div id="productsSlider" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
      @foreach ($products as $product)
       @if ($tempIterator == 0)
         <div class="carousel-item text-center active">
       @else
         <div class="carousel-item text-center" >
       @endif
        <img style="height:320px;widht:320px;" src="{{ asset('uploads/'.$product->image) }}" alt="First slide">
        <div class="carousel-caption d-none d-md-block" style="background-color: white; opacity:0.7;">
          <h5 style="color:#000;">{{$product->name}}</h5>
          <p style="color:#000;">{{str_limit($product->description)}}</p>
        </div>
      </div>
      @php
         $tempIterator++;
       @endphp
      @endforeach
      </div>
      <a class="carousel-control-prev" href="#productsSlider" role="button" data-slide="prev">
        <span class="left fa fa-chevron-left btn btn-success" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#productsSlider" role="button" data-slide="next">
        <span class="right fa fa-chevron-right btn btn-success" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      </div>
    </div>
  </div>
  @php
  // $data =  $products->splice(0,2);
  // dd($products);
  // $iteratorSlide = 0;
  $iteratoreActive = 0 ;
  @endphp
  <div class="container">
    <div class="row mt-5">

    <div class="col-md-9">
                <h3>
            Trending Products
                </h3>
    </div>
   {{-- @php
     dd($trending->count() == 4);
   @endphp --}}
    <div class="col-md-3">
                    <!-- Controls -->
                    @if ($trending->count() >= 4)
                    <div class="controls float-right">
                        <a class="left fa fa-chevron-left btn btn-success" href="#trendingSlider" data-slide="prev"></a>
                        <a class="right fa fa-chevron-right btn btn-success" href="#trendingSlider" data-slide="next"></a>
                    </div>
                    @endif
    </div>
    </div>
     <div id="trendingSlider" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        @foreach ($trending as $product)
             @if ($iteratoreActive == 0)
               <div class="carousel-item active">
             @else
               <div class="carousel-item">
             @endif
             @php
             $iteratoreActive++;
             $tempItems = $trending->splice(0,3);
             // dd();
             @endphp
              <div class="row">
            @foreach ($tempItems as $tempItem)
              <div class="card col-md-4 p-0 rounded-0 border-light">
                <img class="card-img-top" style="height:320px;widht:320px;" src="{{ asset('uploads/'.$tempItem->image) }}" alt="arwi">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item ">
                    <h3>{{$tempItem->name}}</h3>
                  </li>
                  <li class="list-group-item">$: {{$tempItem->price}}</li>
                  <li class="list-group-item p-0">
                    <form class="d-inline" action="{{ route('shoppingCart.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="id" value="{{$tempItem->id}}">
                        <button type="submit" class="btn btn-secondary btn-block" name="submit">Add To Cart</button>
                    </form>
                  </li>
                </ul>
              </div>
            @endforeach
          </div>
        </div>
            @php
              if ($trending->count() == 0) {
                break;
              }
            @endphp
        @endforeach
      </div>
      {{-- <a class="carousel-control-prev" href="#trendingSlider" role="button" data-slide="prev">
        <span class="left fa fa-chevron-left btn btn-success" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#trendingSlider" role="button" data-slide="next">
        <span class="right fa fa-chevron-right btn btn-success" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a> --}}
      </div>
    </div>

@endsection
