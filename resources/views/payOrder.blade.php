@extends('layouts.master')
@section('content')
  @if (Session::has('status'))
   <div class="d-flex justify-content-center alert alert-success">
    <h4>{{session::get('status')}}</h4>
   </div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
               @endforeach
           </ul>
    </div>
  @endif
  <div class="container">
    <div class="col-md-6 mb-4">

    <a href="{{route('payPal')}}" class="btn btn-primary ">Pay with PayPal</a>
   </div>
    <form  action="{{route('checkout')}}" method="post">
      {{ csrf_field() }}
    <div class="col-md-12">
        <div class="row mb-4">
        <div class='col-md-5'>
          <h6>
            <input type="radio" class="form-group" name="selector" checked>
            Pay securely Using your credit card.
          </h6>
            <div class="row">
              <div class="form-group col-md-12">
                <label for="">Card Number *</label>
                <input type="text" required name="cardNumber" @if ($user->cardNumber != null) value="{{$user->cardNumber}}" @else value="{{old('cardNumber')}}" @endif  class="form-control" id="" placeholder="**** **** **** ****">
              </div>
              <div class="form-group col-md-6">
                <label for="">Expiry (Y-M) *</label>
                <input type="text" required name="expiry" @if ($user->expiry != null) value="{{$user->expiry}}" @else value="{{old('expiry')}}" @endif class="form-control" id="" placeholder="2000-01">
              </div>
              <div class="form-group col-md-6">
                <label for="">Card Code *</label>
                <input type="text" required name="cvv"  @if ($user->cvv != null) value="{{$user->cvv}}" @else value="{{old('cvv')}}" @endif class="form-control" id="" placeholder="CVV">
              </div>
            </div>
        </div>
        <div class='col-md-5'>
          <div class="container">
            <h3>Order Summary</h3>
                <span style="font-size: 12px; line-height: 19px;">Subtotal ({{$totalItems}} items and shipping fee included)</span>   <span style="float:right">${{$price}}</span>
                <div class="mt-2">
                  Total Amount <label style="float:right; font-size:18px;">${{$price}}</label>

                </div>
          </div>
        </div>
        </div>
      {{-- <br> --}}
      <div class="text-center">
        <button type="submit" class="btn btn-secondary btn-block">Proceed</button>
      </div>
    </div>

  </form>

  </div>
@endsection
