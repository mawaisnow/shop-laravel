<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap admin template">
  <meta name="author" content="">
  <title>oShop Dashboard</title>
  <link rel="apple-touch-icon" href="{{asset('dashboard/assets/images/apple-touch-icon.png')}}">
  <link rel="shortcut icon" href="{{asset('dashboard/assets/images/favicon.ico')}}">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{asset('dashboard/global/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/css/bootstrap-extend.min.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/assets/css/site.min.css')}}">
  <!-- Plugins -->
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/animsition/animsition.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/asscrollable/asScrollable.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/switchery/switchery.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/intro-js/introjs.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/slidepanel/slidePanel.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/flag-icon-css/flag-icon.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/waves/waves.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/datatables.net-bs4/dataTables.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/datatables.net-responsive-bs4/dataTables.responsive.bootstrap4.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/datatables.net-buttons-bs4/dataTables.buttons.bootstrap4.css')}}">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
  <!-- Fonts -->
  <link rel="stylesheet" href="{{asset('dashboard/global/fonts/material-design/material-design.min.css')}}">
  <link rel="stylesheet" href="{{asset('dashboard/global/fonts/brand-icons/brand-icons.min.css')}}">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="{{asset('dashboard/global/vendor/dropify/dropify.css')}}">
  <!--[if lt IE 9]>
    <script src="{{asset('dashboard/global/vendor/html5shiv/html5shiv.min.js')}}"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="{{asset('dashboard/global/vendor/media-match/media.match.min.js')}}"></script>
    <script src="{{asset('dashboard/global/vendor/respond/respond.min.js')}}"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="{{asset('dashboard/global/vendor/breakpoints/breakpoints.js')}}"></script>
  <script>
  Breakpoints();
  </script>
</head>
<body class="animsition">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
  <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
      <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
      data-toggle="menubar">
        <span class="sr-only">Toggle navigation</span>
        <span class="hamburger-bar"></span>
      </button>
      <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
      data-toggle="collapse">
        <i class="icon md-more" aria-hidden="true"></i>
      </button>
      <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu">
        <img class="navbar-brand-logo" src="{{asset('dashboard/assets/images/logo.png')}}" title="Remark">
        <span class="navbar-brand-text hidden-xs-down"> Remark</span>
      </div>
      <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
      data-toggle="collapse">
        <span class="sr-only">Toggle Search</span>
        <i class="icon md-search" aria-hidden="true"></i>
      </button>
    </div>
    <div class="navbar-container container-fluid">
      <!-- Navbar Collapse -->
      <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
        <!-- Navbar Toolbar -->
        <ul class="nav navbar-toolbar">
          <li class="nav-item hidden-float" id="toggleMenubar">
            <a class="nav-link" data-toggle="menubar" href="#" role="button">
              <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
            </a>
          </li>
      </ul>
        <!-- End Navbar Toolbar -->
        <!-- Navbar Toolbar Right -->
        <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
          <li class="nav-item dropdown">
            <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
            data-animation="scale-up" role="button">
              <span class="avatar avatar-online">
                <img src="{{asset('dashboard/global/portraits/5.jpg')}}" alt="...">
                <i></i>
              </span>
            </a>
            <div class="dropdown-menu" role="menu">
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-card" aria-hidden="true"></i> Billing</a>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-settings" aria-hidden="true"></i> Settings</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="javascript:void(0)" role="menuitem"><i class="icon md-power" aria-hidden="true"></i> Logout</a>
            </div>
          </li>
      </ul>
        <!-- End Navbar Toolbar Right -->
      </div>
      <!-- End Navbar Collapse -->
      <!-- Site Navbar Seach -->
      <div class="collapse navbar-search-overlap" id="site-navbar-search">
        <form role="search">
          <div class="form-group">
            <div class="input-search">
              <i class="input-search-icon md-search" aria-hidden="true"></i>
              <input type="text" class="form-control" name="site-search" placeholder="Searchdashboard.">
              <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
              data-toggle="collapse" aria-label="Close"></button>
            </div>
          </div>
        </form>
      </div>
      <!-- End Site Navbar Seach -->
    </div>
  </nav>
  <div class="site-menubar">
    <div class="site-menubar-body">
      <div>
        <div>
          <ul class="site-menu" data-plugin="menu">
            <li class="site-menu-category">General</li>
            <li class="site-menu-item">
              <a class="animsition-link" href="{{ route('admin.categories.index') }}">
                <i class="site-menu-icon fas fa-tags" aria-hidden="true"></i>
                <span class="site-menu-title">Categories</span>
              </a>
            </li>

            <li class="site-menu-item">
              <a class="animsition-link" href="{{ route('admin.products.index') }}">
                <i class="site-menu-icon fab fa-product-hunt" aria-hidden="true"></i>
                <span class="site-menu-title">Products</span>
              </a>
            </li>
           </ul>
        </div>
      </div>
    </div>
  </div>
  <!-- Page -->
  <div class="page">
    @yield('body')
  </div>
  <!-- End Page -->
    <!-- Core  -->
  <script src="{{asset('dashboard/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>

  <script src="{{asset('dashboard/global/vendor/jquery/jquery.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/tether/tether.js')}}"></script>

  <script src="{{ asset('js/select2.min.js') }}" ></script>

  <script src="{{asset('dashboard/global/vendor/bootstrap/bootstrap.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/animsition/animsition.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/waves/waves.js')}}"></script>
  <!-- Plugins -->
  <script src="{{asset('dashboard/global/vendor/switchery/switchery.min.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/intro-js/intro.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/screenfull/screenfull.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
  <!-- Scripts -->
  <script src="{{asset('dashboard/global/js/State.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Component.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Plugin.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Base.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Config.js')}}"></script>
  <script src="{{asset('dashboard/assets/js/Section/Menubar.js')}}"></script>
  <script src="{{asset('dashboard/assets/js/Section/GridMenu.js')}}"></script>
  <script src="{{asset('dashboard/assets/js/Section/Sidebar.js')}}"></script>
  <script src="{{asset('dashboard/assets/js/Section/PageAside.js')}}"></script>
  <script src="{{asset('dashboard/assets/js/Plugin/menu.js')}}"></script>
  <script src="{{asset('dashboard/global/js/config/colors.js')}}"></script>
  {{-- <script src="{{asset('dashboard/assets/js/config/tour.js')}}"></script> --}}
  <script src="{{ asset('dashboard/global/vendor/datatables.net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-scroller/dataTables.scroller.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-select-bs4/dataTables.select.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-responsive/dataTables.responsive.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js') }}"></script>
  <script src="{{ asset('dashboard/global/vendor/datatables.net-buttons/dataTables.buttons.js') }}"></script>

  <script>
  Config.set('assets', '{{asset('assets')}}');
  </script>
  <!-- Page -->
  <script src="{{asset('dashboard/assets/js/Site.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Plugin/asscrollable.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Plugin/slidepanel.js')}}"></script>
  <script src="{{asset('dashboard/global/js/Plugin/switchery.js')}}"></script>
  <script src="{{asset('dashboard/global/vendor/dropify/dropify.min.js')}}"></script>

  @yield('js')

  <script>
  (function(document, window, $) {
    'use strict';
    var Site = window.Site;
    $(document).ready(function() {
      Site.run();
    });
  })(document, window, jQuery);
  </script>
</body>
</html>
