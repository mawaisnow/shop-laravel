
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('favicon.png') }}">

    <title>oShop</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('main.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  </head>

  <body>

    <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      <h5 class="my-0 mr-md-auto font-weight-normal">
        <a href="{{ route('root') }}">
                  <img src="{{ asset('favicon.png') }}" class="img-fluid logo" alt="oShop">
        </a>
      </h5>
      <nav class="my-2 my-md-0 mr-md-3">
        <a class="p-2 text-dark" href="{{ route('products.index') }}">Products</a>
        <a class="p-2 text-dark" href="{{ route('shoppingCart.index') }}">Shoping Cart</a>
        <a class="p-2 text-dark" href="{{ route('contact') }}">Contact</a>
        {{-- <a class="p-2 text-dark" href="#">About</a> --}}
      </nav>
      @auth ()
        <a  class="p-2 text-dark" href="{{ route('logout') }}"
                     onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();" >Logout</a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
           @csrf
       </form>
      @else
        <div>

          <a class="btn btn-outline-primary" href="{{ route('login') }}">Sign in</a>
          <a class="btn btn-outline-primary" href="{{ route('register') }}">Register</a>
        </div>
      @endauth
    </div>
    @yield('content')
    <div class="container">
      <footer class="pt-4 my-md-5 pt-md-5">
        <div class="row">
          <div class="col-12 col-md">
            <img class="mb-2" src="../../assets/brand/bootstrap-solid.svg" alt="" width="24" height="24">
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
          </div>
          <div class="col-6 col-md">
            <h5>Features</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Cool stuff</a></li>
              <li><a class="text-muted" href="#">Random feature</a></li>
              <li><a class="text-muted" href="#">Team feature</a></li>
              <li><a class="text-muted" href="#">Stuff for developers</a></li>
              <li><a class="text-muted" href="#">Another one</a></li>
              <li><a class="text-muted" href="#">Last time</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Resources</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Resource</a></li>
              <li><a class="text-muted" href="#">Resource name</a></li>
              <li><a class="text-muted" href="#">Another resource</a></li>
              <li><a class="text-muted" href="#">Final resource</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Team</a></li>
              <li><a class="text-muted" href="#">Locations</a></li>
              <li><a class="text-muted" href="#">Privacy</a></li>
              <li><a class="text-muted" href="#">Terms</a></li>
            </ul>
          </div>
        </div>
      </footer>

    </div>
   <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="{{ asset('popper.min.js') }}"></script>
    <script src="{{ asset('bootstrap.min.js') }}"></script>
    <script>

    function isValidEmailAddress(email)
    {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
    function validateEmail()
    {
        var email = $('#guestEmail').val();

        if (email.length < 3 || !isValidEmailAddress(email)) {
         alert('Please enter a valid email');
         return 0;
         }
     $('#guestForm').submit();
    }
    @if ($errors->has('email'))
      @if ($errors->first('email') == 'The email has already been taken.')
      $(document).ready(function(){
        $('#exampleModalCenter').modal('show');
      });
      @endif
    @endif
    </script>

  </body>
</html>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       {{-- <div class="modal-header invitation-header-pop-up">
        <h5 class="modal-title invitation-title" id="exampleModalLabel">Registration Questions</h5>
      </div> --}}
      <div class="modal-body modal-body-invitation">
        {{--
        @php
          $increment = 0;
        @endphp
           @foreach ($event->questions as $question)
                       @php
                         $increment++;
                       @endphp
                       @if($increment == 1)

                       <label style = "display: block;" class="form-control-label"><b>{{$question->question}}</b></label>
                      @else
                         <label style = "display: block; margin-top : 7px;" class="form-control-label"><b>{{$question->question}}</b></label>
                       @endif
                       @foreach($question->answers as $questionAnswer)
                       <input type="text" style = "display: block; width : 300px;" value = "{{$questionAnswer->answer}}" disabled>
                       @endforeach
            @endforeach
      </div>
             {{--@foreach($question->answers as $answer)
             @endforeach --}}
             {{-----<br> --}}
             <div class="appended-modal-body-invitation" id="modelBody">
               Do you want to clear the cart?
             </div>
      <div class="modal-footer invitation-footer-pop-up">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</div>
