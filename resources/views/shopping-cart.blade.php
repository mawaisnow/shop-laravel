@extends('layouts.master')
@section('content')
  {{-- @php
    dd($items);
  @endphp --}}
  @if (Session::has('status'))
   <div class="d-flex justify-content-center alert alert-success">
    <h4>{{session::get('status')}}</h4>
   </div>
   @endif
   @if ($errors->any())
    <div class="alert alert-danger">
           <ul>
               @foreach ($errors->all() as $error)
                   <li>{{ $error }}</li>
               @endforeach
           </ul>
    </div>
  @endif
  <div class="container">
    @isset($items)

    @if ($items->count() != 0)

    <div class="card-deck mb-3 shadow-sm">
      <div class="col-md-12 p-3">
         @if($items->count() == 1)
        <h3 class="float-left">You have {{$items->count()}} item in your cart</h3>
        @elseif($items->count() == 0)
          <h3 class="float-left">Cart is empty</h3>
        @elseif($items->count() > 1)
        <h3 class="float-left">You have {{$items->count()}} items in your cart</h3>
        @endif
        {{--Remove comment to show modal--}}
        <a  href = "{{ route('clearShoppingCart') }}" {{-- data-toggle="modal" data-target="#myModal" --}} class="p-2 text-dark float-right" >Clear Shopping Cart</a>
      </div>
      <div class="col-md-12 border-top d-flex p-2">
        <div class="col-md-1">
          {{-- img --}}
        </div>
        <div class="col-md-3">
          <h5>Product</h5>
        </div>
        <div class="col-md-3">
          <h5>Quantity</h5>
        </div>
        <div class="col-md-2">
          {{-- Edit Cart --}}
        </div>
        <div class="col-md-4">
          <h5>Price</h5>
        </div>
      </div>
      @foreach($items as $item)
       <div class="col-md-12 border-top d-flex p-2">
        <div class="col-md-1">
          <img src = "{{URL::asset('uploads/'.$item->image)}}" class="rounded-circle img-thumbnail" alt="">
        </div>
        <div class="col-md-3 pt-2">
          <form action="{{ route('shoppingCart.destroy',$item->itemId) }}" style="display:inline" onsubmit = "return validation()" method="post">
                {{ csrf_field() }}
               <input type='hidden' name='_method' value="delete" />
               <button type="submit" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
                 title="Delete Item">
                 <i class="fas fa-times" aria-hidden="true"></i>
               </button>
          </form>
         {{$item->name}}
        </div>
        <div class="col-md-3 pt-2 pl-4">
          <form action="{{ route('shoppingCart.update',$item->itemId) }}" style="display:inline" method="post">
                {{ csrf_field() }}
                <input type='hidden' name='_method' value="PUT" />
                <input type="number" class="form-control col-md-12" name="quantity" value = "{{ $item->itemQuantity }}" min="1" >
        </div>
        <div class="col-md-2 pt-2 pl-4">
          <button type="submit" class="btn btn-sm btn-icon btn-pure btn-default" data-toggle="tooltip"
            title="Edit Item">
            <i class="fas fa-edit" aria-hidden="true"></i>
          </button>
        </div>
       </form>
        <div class="col-md-4 pt-2">
          {{$item->price}} x {{$item->itemQuantity}} = {{$item->price * $item->itemQuantity}}
        </div>
      </div>
    @endforeach
  </div>
  <div class="float-right">
  <a href=" {{ route('shipping') }}" class="btn btn-secondary btn-block">Proceed to Checkout</a>
  </div>

    @else
       <div class="col-md-12 text-center p-5 ">
            <h3>Cart is empty</h3>
       </div>
       <div class="float-right">
       <a href=" {{ route('products.index') }}" class="btn btn-secondary btn-block">Continue Shopping</a>
       </div>
    @endif
  @endisset
  @if (!isset($items))
    <div class="col-md-12 text-center p-5 ">
         <h3>Cart is empty</h3>
    </div>
    <div class="float-right">
    <a href=" {{ route('products.index') }}" class="btn btn-secondary btn-block">Continue Shopping</a>
    </div>
  @endif

</div>
<script type = "text/javascript">
function validation() {
  if (confirm("Are you sure want to delete?")) {
         return true;
  }
         return false;
}
});
</script>
@endsection
