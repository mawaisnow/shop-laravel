@extends('layouts.master')

@section('content')
  <div class="container">
    <div class="card-deck mb-3 text-center">

      @foreach ($products as $product)
        @php
          // dd($product);
        @endphp
        <div class="card col-md-4 p-0 rounded-0 border-light">
          <img class="card-img-top" style="height:320px;widht:320px;" src="{{ asset('uploads/'.$product->image) }}" alt="arwi">
          <ul class="list-group list-group-flush">
            <li class="list-group-item pl-0 pr-0 border-top-0">
              <h4>{{$product->name}}</h4>
            </li>
            <li class="list-group-item">$: {{$product->price}}</li>
            <li class="list-group-item p-0">
              <form class="d-inline" action="{{ route('shoppingCart.store') }}" method="post">
                  @csrf
                  <input type="hidden" name="id" value="{{$product->id}}">
                  <button type="submit" class="btn btn-secondary btn-block" name="submit">Add To Cart</button>
              </form>
            </li>
          </ul>
        </div>
      @endforeach




    </div>

  </div>

@endsection
