@extends('layouts.dashboard')
@section('body')
<div class="page-content">
  <div class="row">

    <div class="col-md-4">
        <div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h3 class="panel-title">Primary Panel</h3>
            </div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                odio. Praesent libero. Sed cursus ante dapibus diam.</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h3 class="panel-title">Primary Panel</h3>
            </div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                odio. Praesent libero. Sed cursus ante dapibus diam.</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h3 class="panel-title">Primary Panel</h3>
            </div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                odio. Praesent libero. Sed cursus ante dapibus diam.</div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-primary panel-line">
            <div class="panel-heading">
                <h3 class="panel-title">Primary Panel</h3>
            </div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec
                odio. Praesent libero. Sed cursus ante dapibus diam.</div>
        </div>
    </div>


  </div>

</div>
@endsection
