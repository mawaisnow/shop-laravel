@extends('layouts.dashboard')
@section('body')
<style>
</style>
 @if (Session::has('status'))
  <div class="d-flex justify-content-center alert alert-success">
   <h4>{{session::get('status')}}</h4>
  </div>
  @endif
  @if ($errors->any())
   <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
   </div>
 @endif
<div class="row">
  <div class="col-md-8">
    <!-- Panel Static Labels -->
    <div  class="panel  panel-primary panel-line" style="margin:10px;">
      <h3 class="panel-title panel-heading">Products</h3>
      <div class="panel-body container-fluid">
        <div class="table-responsive">
            <table class="table table-hover dataTable table-striped w-full" id="products">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Category</th>
                  <th>Description</th>
                  <th>Price</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
               @foreach ($products as $product)
                <tr>
                  <td>{{$product->name}}</td>

                  <td>{{$product->quantity}}</td>

                  <td>{{$product->categoryName}}</td>

                  <td><div style="overflow-y: scroll;height: 80px;">{{$product->description}}</div></td>

                  <td>{{$product->price}}</td>
                  <td> <img style = "width : 80px; height: 80px;" src = "{{URL::asset('uploads/'.$product->image)}}" alt = "Not Found"></td>
                  <td style="width: 55px;">
                    <a  title="Edit Product" class = "fas fa-edit" href="{{route('admin.products.edit',$product->id)}}">
                    </a>
                  <form action="{{route('admin.products.destroy',$product->id)}}" style="display:inline" onsubmit="return validation()" method="post">
                        {{ csrf_field() }}
                       <input type='hidden' name='_method' value="delete" />
                       <button type="submit" class="btn btn-sm btn-danger btn-icon btn-pure btn-default" data-toggle="tooltip"
                         title="Delete Product">
                         <i class="fas fa-times" aria-hidden="true"></i>
                       </button>
                  </form>
                  </td>
                </tr>
              @endforeach
              </tbody>
          </table>
          </div>
        {{-- </div> --}}
      </div>
    </div>
    <!-- End Panel Static Labels -->
  </div>
  <div class="col-md-4">
    <!-- Panel Floating Labels -->
    <div  class="panel  panel-primary panel-line" style="margin:10px;">
      <div class="panel-heading">
        @if (isset($productForEdit))
          <h3 class="panel-title">Edit Product</h3>
        @else
          <h3 class="panel-title">Add Product</h3>
        @endif
      </div>
      <div class="panel-body col-md-12 ">
                    <div class="col-md-12"  id="formDiv">
                    @if (isset($productForEdit))
                      {!! Form::open(array('route' => ['admin.products.update','id'=>$productForEdit->id], 'method' => 'PUT','style'=>'width:100%;','id'=>'formmain','enctype' => 'multipart/form-data')) !!}
                      <input type='hidden' name='_method' value="PUT" />
                    @else
                      {!! Form::open(array('route' => 'admin.products.store', 'method' => 'POST', 'style'=>'width:100%', 'enctype' => 'multipart/form-data')) !!}
                    @endif
                         {{ csrf_field() }}

                    <div class="QDiv0 mt-20" style="margin-left: -15px;">
                    <div class=" col-md-50 mt 10" >

                        @if (isset($productForEdit))
                          <div class="form-group form-material">
                          <input type="text" style="margin-top: -25px;"  class="form-control"  name="name" value="{{$productForEdit->name}}" placeholder="Enter Product Name"/>
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group form-material">
                          <input type="number" min="1" style="margin-top: -25px;"  class="form-control"  name="quantity" value="{{$productForEdit->quantity}}" placeholder="Quantity" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group form-material">
                          <textarea style="margin-top: -25px;"  class="form-control"  name="description" placeholder="Description">{{$productForEdit->description}}</textarea>
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group form-material">
                          <input type="text" style="margin-top: -25px;"  class="form-control"  name="price" value="{{$productForEdit->price}}" placeholder="Price" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group">
                          <label class="form-control-label" style="color: #757575; font-weight: 500;" for="image">Image</label>
                          <input type="file"  id="image" name="image"  data-plugin="dropify" accept = "image/png,image/jpg,image/jpeg" value=""/>
                          </div>
                          <div class='checkbox-custom checkbox-default'>
                            <input type='checkbox' name='is_trending' value='1' @if($productForEdit->is_trending == 1) checked @endif >
                            <label>
                              Is_Trending
                            </label>
                          </div>
                          <div class="form-group form-material" id ="categoriesSelect">
                            <label class="form-control-label" for="categories">Categories</label>
                            <select id="categories" class="form-control" name="categoryId">
                              @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        @else
                          <div class="form-group form-material">
                          <input type="text" style="margin-top: -25px;"  class="form-control"  name="name" value="" placeholder="Name" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group form-material">
                          <input type="number" min="1" style="margin-top: -25px;"  class="form-control"  name="quantity" value="" placeholder="Quantity" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group form-material">
                          <textarea style="margin-top: -25px;"  class="form-control"  name="description" placeholder="Description"></textarea>
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class="form-group form-material">
                          <input type="text" style="margin-top: -25px;"  class="form-control"  name="price" value="" placeholder="Price" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div  class="form-group">
                          <label class="form-control-label" style="color: #757575; font-weight: 500;" for="image">Image</label>
                          <input type="file" id="image" data-plugin="dropify" name="image" accept = "image/png,image/jpg,image/jpeg" value=""/>
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                          </div>
                          <div class='checkbox-custom checkbox-default'>
                            <input type='checkbox' name='is_trending' value='1' >
                            <label>
                              Is_Trending
                            </label>
                          </div>
                          <div class="form-group form-material" id ="categoriesSelect">
                            <label class="form-control-label" for="categories">Categories</label>
                            <select id="categories" class="form-control" name="categoryId">
                              @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        @endif

                    </div>
                  </div>
                    <div class="col-md-40 " name="second-element">
                        {!! Form::button('Submit',['type'=>'submit','style'=>'background:#6c406e; float:right; color: white; hover: red;', 'class'=>' btn btn-secondary', 'id'=>'submit']) !!}
                    </div>
                    {!! Form::close() !!}
                  </div>
        </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function() {

   $('#products').DataTable();
   $("#categories").select2({
   });
});
  function validation() {
    if (confirm("Are you sure want to delete?")) {
           return true;
    }
           return false;
  }
</script>
@endsection
