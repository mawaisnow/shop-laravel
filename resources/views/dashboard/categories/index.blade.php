@extends('layouts.dashboard')
@section('body')
  <script type="text/javascript">
  </script>
  <style>
 .category-padding{
    padding-right : 160px !important;
  }
  </style>
  @if (Session::has('status'))
  <div class="d-flex justify-content-center alert alert-success">
   <h4>{{session::get('status')}}</h4>
  </div>
  @endif
  @if ($errors->any())
   <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
   </div>
 @endif
<div class="row">
  <div class="col-md-8">
    <!-- Panel Static Labels -->
    <div class="panel panel-primary panel-line" style="margin:10px;">
      <h3 class="panel-title panel-heading">Categories</h3>
      <div class="panel-body container-fluid">
        <div class="table-responsive">
            <table class="table table-hover dataTable table-striped w-full" id="categories">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Action</th>
               </tr>
              </thead>
              <tbody>
               @foreach ($categories as $category)
                <tr>
                  <td class = "text-overflow">{{str_limit($category->name,100)}}</td>
                  <td>
                    <a  title="Edit Category" class = "fas fa-edit" href="{{route('admin.categories.edit',$category->id)}}">
                    </a>
                    <form action="{{route('admin.categories.destroy',$category->id)}}" style="display:inline" onsubmit="return validation()" method="post">
                          {{ csrf_field() }}
                         <input type='hidden' name='_method' value="delete" />
                         <button type="submit" data-toggle="tooltip" class="btn btn-sm btn-danger btn-icon btn-pure btn-default" data-original-title="Delete Product">
                           <i class="fas fa-times" aria-hidden="true"></i>
                         </button>
                    </form>
                  </td>
               </tr>

                @endforeach
              </tbody>
            </table>
          </div>
        {{-- </div> --}}
      </div>
    </div>
    <!-- End Panel Static Labels -->
  </div>
  <div class="col-md-4">
    <!-- Panel Floating Labels -->
    <div class="panel  panel-primary panel-line" style="margin:10px;">
      <div class="panel-heading">
        @if (isset($categoryForEdit))
          <h3 class="panel-title">Edit Category</h3>
        @else
          <h3 class="panel-title">Add Category</h3>
        @endif
      </div>
      <div class="panel-body col-md-12 ">
                    <div class="col-md-12"  id="formDiv">
                    @if (isset($categoryForEdit))
                      {!! Form::open(array('route' => ['admin.categories.update','id'=>$categoryForEdit->id], 'method' => 'PUT','style'=>'width:100%;','id'=>'formmain', 'enctype' => 'multipart/form-data')) !!}
                      <input type='hidden' name='_method' value="PUT" />
                    @else
                      {!! Form::open(array('route' => 'admin.categories.store', 'method' => 'POST', 'style'=>'width:100%')) !!}
                    @endif
                         {{ csrf_field() }}

                    <div class="QDiv0 mt-20" style="margin-left: -15px;">
                    <div class=" col-md-50 mt 10" >
                      <div class="form-group form-material">
                        @if (isset($categoryForEdit))
                          <input type="text" style="margin-top: -25px;"  class="form-control"  name="name" value="{{$categoryForEdit->name}}" placeholder="Enter Category Name" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                        @else
                          <input type="text" style="margin-top: -25px;"  class="form-control" name="name" value="" placeholder="Enter Category Name" />
                          <span style="color:red; float:right; margin-top:-25px;">*</span>
                        @endif
                       </div>
                    </div>
                  </div>
                    <div class="col-md-40 " name="second-element">
                        {!! Form::button('Submit',['type'=>'submit','style'=>'background:#6c406e; float:right; color: white; hover: red;', 'class'=>' btn btn-secondary', 'id'=>'submit']) !!}
                    </div>
                    {!! Form::close() !!}
                  </div>
        </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function() {

   $('#categories').DataTable();
});
 function validation() {
      if (confirm("Are you sure want to delete?")) {
             return true;
      }
             return false;
  }
</script>
@endsection
