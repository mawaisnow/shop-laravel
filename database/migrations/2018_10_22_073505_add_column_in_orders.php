<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('payerId','255')->default(0)->after('userId');
            $table->string('transactionId','255')->default(0)->after('payerId');
            $table->string('merchantType','255')->default('authorize.net')->after('transactionId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
          $table->dropColumn('payerId');
          $table->dropColumn('transactionId');
          $table->dropColumn('merchantType');
        });
    }
}
