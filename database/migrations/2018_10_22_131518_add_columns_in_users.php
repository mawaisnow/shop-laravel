<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cardNumber','255')->default('')->after('password');
            $table->string('expiry','255')->default('')->after('cardNumber');
            $table->string('cvv','255')->default('')->after('expiry');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('cardNumber');
            $table->dropColumn('expiry');
            $table->dropColumn('cvv');
        });
    }
}
