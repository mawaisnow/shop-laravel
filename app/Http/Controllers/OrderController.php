<?php

namespace App\Http\Controllers;
use Auth;
use App\ShoppingCart;
use App\CartItem;
use App\Order;
use App\OrderItem;
use App\ShippingInfo;
use App\User;
use Session;
use Mail;
use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
    public function shippmentDetails(Request $request)
    {

      $request->validate([
        'fullName' => 'required|string|max:255',
        'phone' => 'required|numeric|digits_between:10,14',
        'city' => 'required|alpha|max:255',
        'address' => 'required|string'
      ]);
      // shipping detail
      $userId = Auth::user()->id;
      $shippingInfo = ShippingInfo::where('userId','=',$userId)->first();
      if ($shippingInfo) {
        $shippingInfo->userId = $userId;
        $shippingInfo->fullName = $request->fullName;
        $shippingInfo->phone = $request->phone;
        $shippingInfo->city = $request->city;
        $shippingInfo->address = $request->address;
        $shippingInfo->save();
      } else {
        // code...
        $shippingInfo = new ShippingInfo;
        $shippingInfo->userId = $userId;
        $shippingInfo->fullName = $request->fullName;
        $shippingInfo->phone = $request->phone;
        $shippingInfo->city = $request->city;
        $shippingInfo->address = $request->address;
        $shippingInfo->save();
      }
      // for purchase
      return redirect(route('payments'));
    }

    public function payments()
    {
      $userId = Auth::user()->id;
      $user = User::where('id','=',$userId)->first();
      $data = $this->cartData();
      $total = 0;
      foreach($data['items'] as $tempItem) {
        $total += $tempItem['qty'];
      }
      $totalItems = $total;
      $price = $data['total'];
      return view('payOrder',compact('user','totalItems','price'));
    }
    public function payWithAuthorize(Request $request)
    {
       $request->validate([
           'cardNumber' => 'required|numeric|digits_between:16,16',
           'expiry' => 'required|date_format:Y-m',
           'cvv' => 'required|numeric|digits_between:3,3'
       ]);

       $user = User::where('id','=',Auth::user()->id)->first();
       $user->cvv = $request->cvv;
       $user->cardNumber = $request->cardNumber;
       $user->expiry = $request->expiry;
       $user->save();
       $data = $this->cartData();
       $price = $data['total'];
      // authorize.net credit card implementaions
      // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(config('services.authorize.login'));
        $merchantAuthentication->setTransactionKey(config('services.authorize.key'));
        $refId = 'ref'.time();
      // Create the payment data for a credit card
          $creditCard = new AnetAPI\CreditCardType();
          $creditCard->setCardNumber($request->cardNumber);
          // $creditCard->setExpirationDate( "2038-12");
          $expiry = $request->expiry;
          $creditCard->setExpirationDate($expiry);
          $paymentOne = new AnetAPI\PaymentType();
          $paymentOne->setCreditCard($creditCard);
          // Create a transaction
          $transactionRequestType = new AnetAPI\TransactionRequestType();
          $transactionRequestType->setTransactionType("authCaptureTransaction");
          $transactionRequestType->setAmount($price);
          $transactionRequestType->setPayment($paymentOne);
          $request = new AnetAPI\CreateTransactionRequest();
          $request->setMerchantAuthentication($merchantAuthentication);
          $request->setRefId( $refId);
          $request->setTransactionRequest($transactionRequestType);
          $controller = new AnetController\CreateTransactionController($request);
          $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
          if ($response != null)
            {
              $tresponse = $response->getTransactionResponse();
              if (($tresponse != null) && ($tresponse->getResponseCode()=="1"))
              {
                // echo "Charge Credit Card AUTH CODE : " . $tresponse->getAuthCode() . "\n";
                // echo "Charge Credit Card TRANS ID  : " . $tresponse->getTransId() . "\n";
                //order place
                $userId = Auth::user()->id;
                $shippingInfo = ShippingInfo::where('userId','=',$userId)->first();
                $shippingId = $shippingInfo->id;
                //order save

                $cart = ShoppingCart::where('userId','=',$userId)->first();
                if ($cart) {
                    $items = CartItem::where('shoppingCartId' , '=' , $cart->id)->join('products','productId','=','products.id')
                        ->select('cart_items.*','price', 'products.quantity as prodQuantity')->get();
                    $price = 0;
                    foreach ($items as $value) {
                      $price += $value->quantity * $value->price;
                    }
                    $order = new Order;
                    $userId = Auth::user()->id;
                    $order->userId = $userId;
                    $order->totalPrice = $price;
                    $order->transactionId = $tresponse->getTransId();
                    $order->merchantType = 'authorize.net';
                    $order->shippingId = $shippingId;
                    $order->save();
                    foreach($items as $item)
                    {
                      // dd($item);
                      $productId = $item->productId;
                      $quantity = $item->quantity;
                      $price = $item->price;
                      $orderItem = new orderItem;
                      $orderItem->productId = $productId;
                      $orderItem->orderId = $order->id;
                      $orderItem->quantity = $quantity;
                      $orderItem->save();
                   }

                    //for clearing Cart
                    $userId = Auth::user()->id;
                    $shoppingitems = ShoppingCart::where('userId','=',$userId)->get();
                    if(count($shoppingitems) == 0)
                    {
                      Session::flash('status' , 'Cart is already cleared');
                      return redirect(route('shoppingCart.index'));
                    }
                    foreach ($shoppingitems as $TempValue) {
                       $items = CartItem::where('shoppingCartId','=',$TempValue->id)->get();
                       if(count($items) > 0)
                       {
                         foreach($items as $item)
                         {
                           $item->delete();
                         }

                       }
                    }
                    $name = Auth::user()->name;
                    $email = Auth::user()->email;
                    $dataForEmail = [
                      'name' => $name,
                    ];

                    Mail::send('emails.orderMessage', $dataForEmail, function ($message) use ($email) {
                       $message->to($email)->subject('Order Confirmation');
                       $message->from('alburraqoshop@gmail.com', 'Oshop');
                     });

                   Session::flash('status', "Checkout Completed Successfully");
                   return redirect(route('shoppingCart.index'));
                }else
                {
                  Session::flash('status', "Cart is empty");
                  return redirect(route('shoppingCart.index'));
                }
              }
              else
              {
                $errors =  $tresponse->getErrors();
                $error = '';
                foreach ($errors as  $value) {
                  $error = ($value->getErrorText());

                }
                Session::flash('status', "$error");
                return redirect(route('shoppingCart.index'));
              }
            }
            else
            {
              Session::flash('status', "Charge Credit Card Null response returned");
              return redirect(route('shoppingCart.index'));
            }

            dd('out of this');


   }
   public function shipping()
   {
     $id = Auth::user()->id;
     $userShippmenet = ShippingInfo::where('userId','=',$id)->first();
     return view('delivery', compact('userShippmenet'));
   }

   public function payWithPaypal()
   {
        //pay with paypal
        $provider = new ExpressCheckout;
        $data = $this->cartData();
        $response = $provider->setExpressCheckout($data);

         return redirect($response['paypal_link']);


         //give a discount of 10% of the order amount
         // $data['shipping_discount'] = round((10 / 100) * $total, 2);
     //
     // dd('out of paypal');
   }
   public function storePayment(Request $request)
   {
     $provider = new ExpressCheckout;

     $token = $request->token;
     $PayerID = $request->PayerID;

     $response = $provider->getExpressCheckoutDetails($token);

     $data = $this->cartData();

     $response = $provider->doExpressCheckoutPayment($data, $token, $PayerID);
     // self::checkout($request);
     // dd($response);
     // place order
     $userId = Auth::user()->id;
     $shippingInfo = ShippingInfo::where('userId','=',$userId)->first();
     $shippingId = $shippingInfo->id;
     //order save

     $cart = ShoppingCart::where('userId','=',$userId)->first();
     if ($cart) {
         $items = CartItem::where('shoppingCartId' , '=' , $cart->id)->join('products','productId','=','products.id')
             ->select('cart_items.*','price', 'products.quantity as prodQuantity')->get();
         $price = 0;
         foreach ($items as $value) {
           $price += $value->quantity * $value->price;
         }
         $order = new Order;
         $userId = Auth::user()->id;
         $order->userId = $userId;
         $order->totalPrice = $price;
         $order->shippingId = $shippingId;
         $order->merchantType = 'paypal';
         $order->payerId = $PayerID;
         $order->save();
         foreach($items as $item)
         {
           // dd($item);
           $productId = $item->productId;
           $quantity = $item->quantity;
           $price = $item->price;
           $orderItem = new orderItem;
           $orderItem->productId = $productId;
           $orderItem->orderId = $order->id;
           $orderItem->quantity = $quantity;
           $orderItem->save();
        }

         //for clearing Cart
         $userId = Auth::user()->id;
         $shoppingitems = ShoppingCart::where('userId','=',$userId)->get();
         if(count($shoppingitems) == 0)
         {
           Session::flash('status' , 'Cart is already cleared');
           return redirect(route('shoppingCart.index'));
         }
         foreach ($shoppingitems as $TempValue) {
            $items = CartItem::where('shoppingCartId','=',$TempValue->id)->get();
            if(count($items) > 0)
            {
              foreach($items as $item)
              {
                $item->delete();
              }

            }
         }

         $name = Auth::user()->name;
         $email = Auth::user()->email;
         $dataForEmail = [
           'name' => $name,
         ];

         Mail::send('emails.orderMessage', $dataForEmail, function ($message) use ($email) {
            $message->to($email)->subject('Order Confirmation');
            $message->from('alburraqoshop@gmail.com', 'Oshop');
          });
        Session::flash('status', "Checkout Completed Successfully");
        return redirect(route('shoppingCart.index'));
     }else
     {
       Session::flash('status', "Cart is empty");
       return redirect(route('shoppingCart.index'));
     }

   }
   protected function cartData()
   {
     $data = [];

     $userId = Auth::user()->id;
     $cart = ShoppingCart::where('userId','=',$userId)->first();

         $items = CartItem::where('shoppingCartId' , '=' , $cart->id)->join('products','productId','=','products.id')
             ->select('cart_items.*','price', 'products.quantity as prodQuantity','name')->get();
         $price = 0;
         foreach ($items as $value) {
           $price += $value->quantity * $value->price;
         }
      foreach ($items as $item) {
       $data['items'][] = [
          'name' => $item->name,
          'price' => $item->price,
          'qty' => $item->quantity
       ];
      }
       $total = 0;
        foreach($data['items'] as $tempItem) {
          $total += $tempItem['price'] * $tempItem['qty'];
        }
     $data['invoice_id'] = uniqid();
     $data['invoice_description'] = "Order #{$data['invoice_id']} Invoice";
     $data['return_url'] = route('paymentStore');
     $data['cancel_url'] = route('shoppingCart.index');
     $data['total'] = $total;

     return $data;
   }
}
