<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function guestRegistration(Request $request)
    {
        $request->request->add(['email' => $request->guestEmail]);

        $validator = Validator::make($request->all(), [
          'email'=>'required|email|unique:users',
          ]);

          if ($validator->fails()) {
            return redirect(route('login'))
          		->withErrors($validator)
          		->withInput();
          }
           $email = $request->email;
           $split = explode("@", $email);
           $password = substr(str_shuffle(str_repeat("abcde", 9)), 0, 9);
           $milliseconds = round(microtime(true) * 1000);
           $passwordSet = $password.$milliseconds;
           // $pass = Hash::make("123123");
           $data = [
             'name' => $split[0],
             'email' => $email,
             'password' => $passwordSet,
           ];
           $this->create($data);

           $credentials = [
             'email' => $email,
             'password' => $passwordSet,
           ];

           $dataForEmail = [
             'name' => $split[0],
             'email' => $email,
             'password' => $passwordSet,
           ];

           Mail::send('emails.registrationMessage', $dataForEmail, function ($message) use ($data) {
                $message->to($data['email'])->subject('Registration Information');
                $message->from('alburraqoshop@gmail.com', 'Oshop');
              });

           if (Auth::attempt($credentials)) {
                return redirect()->intended('home');
            }else {
                return redirect()->intended('home');
            }

    }
}
