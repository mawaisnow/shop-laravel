<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\UserMessage;
use Session;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $products = Product::all();
      $trending = Product::where('is_trending','=', 1 )->get();
      // dd($trending);
        return view('home',compact('products','trending'));
    }

    public function contact()
    {
        return view('contact');
    }
    public function contactMessage(Request $request)
    {
      $request->validate([
        'name'=>'required|string|max:255',
        'email'=>'required|string|max:255',
        'message'=>'required|string|max:1000'
      ]);
      $userMessage = new UserMessage;
      $userMessage->name = $request->name;
      $userMessage->email = $request->email;
      $userMessage->message = $request->message;
      $userMessage->save();
      Session::flash('status', "Message recevied Successfully");
      return redirect(route('contact'));
    }
}
