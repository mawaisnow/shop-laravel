<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use App\CartItem;
use App\OrderItem;
use Session;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

          $categories = Category::all();
          return view('dashboard.categories.index' , compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         $request->validate([
            'name' => 'required|string|max:255|unique:categories'
          ]);

        /*
          $this->validate(
                            $request,
                            [
                              'name' => 'required|string|max:255'
                            ],
                            [
                                  'name.unique:categories' => 'Category already exists',
                            ]
                      );
                         */

         $category = new Category;
         $category->name = $request->name;
         $category->save();
         Session::flash("status" , "Category Added Successfully");
         return redirect(route('admin.categories.index'));
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($categoryId)
    {
        $categories = Category::all();
        $categoryForEdit = Category::find($categoryId);
        return view("dashboard.categories.index" , compact("categories" , "categoryForEdit"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $categoryId)
    {

      $request->validate([
         'name' => 'required|string|max:255|unique:categories'
       ]);

      $category = Category::find($categoryId);
      $category->name = $request->name;
      $category->save();
      Session::flash("status" , "Category Updated Successfully");
      return redirect(route('admin.categories.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoryId)
    {

      $products = Product::where('categoryId' , '=' , $categoryId)->get();
      if(count($products) > 0)
      {
         foreach($products as $product)
         {

        $cartItems = CartItem::where('productId', '=', $product->id)->get();

        if (count($cartItems) > 0) {

                foreach ($cartItems as $cartItem) {
                      $cartItem->delete();
                }

            }
            $orderItems = OrderItem::where('productId', '=', $product->id)->get();

            if (count($orderItems) > 0) {

                    foreach ($orderItems as $orderItem) {
                          $orderItem->delete();
                    }
            }
          $productForDelete = Product::find($product->id);
          if(!is_null($productForDelete))
          {

              $productForDelete->delete();
          }

        }

     }

     $categoryForDelete = Category::find($categoryId);
     if(!is_null($categoryForDelete))
     {

         $categoryForDelete->delete();
     }
     
        Session::flash('status', 'Category Deleted Successfully');
        return redirect(route('admin.categories.index'));
    }

}
