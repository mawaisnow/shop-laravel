<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\CartItem;
use App\OrderItem;
use Session;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

        $products = Product::join('categories', 'products.categoryId', '=', 'categories.id')->select('products.*', 'categories.name as categoryName')->get();
        $categories = Category::all();
        return view('dashboard.products.index' , compact('products' , 'categories'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // dd($request);

       $request->validate([
          'name' => 'required|string|max:255|unique:products',
          'quantity' =>'required|integer',
           'description' => 'required|string|max:1500',
           'price' => 'required|integer',
           'categoryId' => 'required',
           'image' => 'required|image'
        ]);

       $product = new Product;
       $product->name = $request->name;
       $product->quantity = $request->quantity;
       $product->description = $request->description;
       $product->price = $request->price;
       $product->categoryId = $request->categoryId;
        if (isset($request->is_trending)) {
          $product->is_trending = $request->is_trending;
        }
       $product->image = $request->file('image')->store('productsImages','media');
       $product->save();
       Session::flash("status" , "Product added successfully");
       return redirect(route('admin.products.index'));

 }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($productId)
  {

      $productForEdit = Product::find($productId);
      $products = Product::join('categories', 'products.categoryId', '=', 'categories.id')->select('products.*', 'categories.name as categoryName')->get();
      $categories = Category::all();
      return view("dashboard.products.index" , compact("products","categories" , "productForEdit"));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $productId)
  {

    $request->validate([
        'name' => 'required|string|max:255',
        'quantity' =>'required|integer',
        'description' => 'required|string|max:1500',
        'price' => 'required|integer',
        'categoryId' => 'required',
        'image' => 'image'
     ]);
     // |unique:products,name'.$this->name.',name'

    $product = Product::find($productId);
    $product->name = $request->name;
    $product->quantity = $request->quantity;
    $product->description = $request->description;
    $product->price = $request->price;
    $product->categoryId = $request->categoryId;
    if (isset($request->is_trending)) {
      $product->is_trending = $request->is_trending;
    }
    if($request->file('image') != null)
    {

       $product->image = $request->file('image')->store('productsImages','media');
    }
    $product->save();
    Session::flash("status" , "Product updated successfully");
    return redirect(route('admin.products.index'));
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($productId)
  {

    $cartItems = CartItem::where('productId', '=', $productId)->get();

    if (count($cartItems) > 0) {

            foreach ($cartItems as $cartItem) {
                  $cartItem->delete();
            }

        }
        $orderItems = OrderItem::where('productId', '=', $productId)->get();

        if (count($orderItems) > 0) {

                foreach ($orderItems as $orderItem) {
                      $orderItem->delete();
                }
        }
      $product = Product::find($productId);
      if(!is_null($product))
      {

          $product->delete();
      }

      Session::flash('status', 'Product Deleted Successfully');
      return redirect(route('admin.products.index'));
  }

}
