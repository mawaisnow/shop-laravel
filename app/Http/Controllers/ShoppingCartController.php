<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\ShoppingCart;
use App\CartItem;
use App\Product;
use Auth;
use Session;

class ShoppingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $cart = ShoppingCart::where('userId' , '=' , Auth::user()->id)->first();
        if($cart != null)
        {
            $items = CartItem::where('shoppingCartId',$cart->id)->join('products', 'cart_items.productId' , '=', 'products.id')->select('cart_items.id as itemId' , 'cart_items.quantity as itemQuantity' , 'cart_items.*' , 'products.*')->get();
        }
        return view('shopping-cart' , compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'id'=>'required|integer'
        ]);
        $productId = $request->id;
        $userId = Auth::user()->id;
        $cart = ShoppingCart::where('userId','=',$userId)->first();
        if ($cart) {

          $cartId = $cart->id;
          $items = CartItem::where('shoppingCartId' , '=' , $cartId)->get();
          foreach ($items as $item) {
               if($item->productId == $productId)
               {
                 $tempItem = CartItem::where('id' , '=' , $item->id)->first();
                 $tempItem->quantity++;
                 $tempItem->save();
                 Session::flash('status' , 'Item updated Successfully');
                 return redirect(route('shoppingCart.index'));
               }
          }
          //if new product
          $newItem = new CartItem;
          $newItem->shoppingCartId = $cartId;
          $newItem->productId = $productId;
          $newItem->quantity = 1;
          $newItem->save();
          Session::flash('status' , 'Item Added Successfully');
          return redirect(route('shoppingCart.index'));
        } else {
           $cart = new ShoppingCart;
           $cart->userId = $userId;
           $cart->save();
           $cartId = $cart->id;
           $newItem = new CartItem;
           $newItem->shoppingCartId = $cartId;
           $newItem->productId = $productId;
           $newItem->quantity = 1;
           $newItem->save();
           Session::flash('status' , 'Item Added Successfully');
           return redirect(route('shoppingCart.index'));

        }
 }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function show(ShoppingCart $shoppingCart)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function edit(ShoppingCart $shoppingCart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $itemId)
    {
        $request->validate([
          'quantity'=>'required'
        ]);
         $cartItem = CartItem::where('id' , '=' , ((int)$itemId))->first();
         $cartItem->quantity = $request->quantity;
         $cartItem->save();
         Session::flash('status' , 'Item Updated Successfully');
         return redirect(route('shoppingCart.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShoppingCart  $shoppingCart
     * @return \Illuminate\Http\Response
     */
    public function destroy($itemId)
    {
        CartItem::find($itemId)->delete();
        Session::flash('status' , 'Item Deleted Successfully');
        return redirect(route('shoppingCart.index'));
    }
    public function clearShoppingCart()
    {
         $userId = Auth::user()->id;
         $shoppingitems = ShoppingCart::where('userId','=',$userId)->get();
         if(count($shoppingitems) == 0)
         {
           Session::flash('status' , 'Cart is already cleared');
           return redirect(route('shoppingCart.index'));
         }
         foreach ($shoppingitems as $TempValue) {
            $items = CartItem::where('shoppingCartId','=',$TempValue->id)->get();
            if(count($items) > 0)
            {
              foreach($items as $item)
              {
                $item->delete();
              }

            }
         }

         Session::flash('status' , 'Cart Cleared Successfully');
         return redirect(route('shoppingCart.index'));
    }
}
